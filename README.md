Colle
======

Have you ever encountered a problem that there are lots of point cards in your wallet? Every time when you have the check, you have to find the point card in your thick wallet which is full of receipts.

In this project, we make use the nfc function of the smart phone. After the check, staff will show the nfc tag and you can scan it to earn point.

![](https://i.imgur.com/bM4zub3.jpg)


Application
===
![](https://i.imgur.com/RJKWmOz.jpg)
![](https://i.imgur.com/9PrgLdf.jpg)
