package com.cy.getpoint;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.widget.Toast;
import com.cy.getpoint.SlidingMenu.ActivityBase;
import com.rey.material.app.Dialog;
import com.rey.material.app.SimpleDialog;
import com.rey.material.app.DialogFragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

public class MainActivity extends ActivityBase {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent it = getIntent();

        if( ServerRequests.isNetworkAvailable(this) ){


            if( NfcAdapter.ACTION_TECH_DISCOVERED.equals(it.getAction()) ){

                UserSession us = new UserSession(this);
                if( !us.isLogin() ){
                    Toast.makeText( this , "請先登入" , Toast.LENGTH_SHORT ).show();
                    Intent login = new Intent(this, Login.class);
                    startActivity(login);
                    finish();
                }
                else{
                    User currentUser = us.getUser(this);
                    HashMap<String,String> map = new HashMap<String,String>();
                    ServerRequests sr = new ServerRequests(this);
                    Tag tag = it.getParcelableExtra( NfcAdapter.EXTRA_TAG );

                    String store_id = ByteArrayToHexString( tag.getId() );
                    Boolean cardExist = false;

                    switch (store_id){
                        case "8BA70CEC":
                        case "5A5AEF7F":
                        case "2A035AB0":
                        case "71649B70":
                        case "2F18E35A": //EASY CARD
                        case "107D8FDC": //Line Card
                            cardExist = true;
                            break;
                        default:
                            customDialog("無法識別" );
                            break;
                    }

                    if (cardExist){
                        map.put("user_id" , currentUser.user_id );
                        map.put("store_id", store_id);

                        JSONObject result;
                        try {
                            result = new JSONObject( sr.postDataAsyncTask("/api/addPoint", map) ) ;

                            if( result.getBoolean("status") ){
                                /** if success , open getPoint and send data for activity **/

                                currentUser = authenticate(currentUser);
                                us.login(currentUser);
                                getPoint(currentUser.cards, store_id);
                            }

                        }
                        catch( JSONException e ){
                            e.printStackTrace();
                        }
                    }
                }

            }
            else if(NfcAdapter.ACTION_TAG_DISCOVERED.equals( it.getAction() )){
                customDialog( "不支援的 TAG 類型。" );
            }
            else{
                UserSession us = new UserSession(this);

                if( us.isLogin() ){
                    Intent card = new Intent(this, Cards.class);
                    Bundle userBundle = new Bundle();

                    User realUser = us.getUser(this);
                    userBundle.putString("cards",realUser.cards);
                    card.putExtras(userBundle);

                    startActivity(card);
                    finish();
                }
                else{
                    Intent login = new Intent(this, Login.class);
                    startActivity(login);
                    finish();
                }
            }
        }
        else{
            customDialog("尚未連接至網路");
        }
    }

    private void customDialog( String okStr ){
        Dialog.Builder builder = new SimpleDialog.Builder( R.style.SimpleDialogLight ){

            public void onPositiveActionClicked( DialogFragment fragment ){
                finish();
            }
        };

        builder.title(okStr)
                .positiveAction("好");

        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), "");
    }


    String ByteArrayToHexString(byte [] inarray)
    {
        int i, j, in;
        String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        String out= "";

        for(j = 0 ; j < inarray.length ; ++j)
        {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }
        return out;
    }

    private User authenticate(User user) {
        ServerRequests serverRequest = new ServerRequests(this);
        return serverRequest.fetchUserDataAsyncTask(user);
    }

    private void getPoint(String cards, String store){
        try {
            JSONArray array = new JSONArray(cards);
            CardRecord cr[] = new CardRecord[array.length()];
            int i = -1;

            for(int n = 0; n < array.length(); n++)
            {
                JSONObject object = array.getJSONObject(n);
                cr[n] = new CardRecord(object);
                if (cr[n].storeID.equals(store))
                    i = n;
            }

            if (i>=0){
                Intent get = new Intent(this,getPoints.class);

                Bundle userBundle = new Bundle();
                userBundle.putString("cards",cards);
                userBundle.putString("object", cr[i].object.toString());
                userBundle.putInt("position", i);

                Toast.makeText(this, "集點成功！", Toast.LENGTH_SHORT).show();

                get.putExtras(userBundle);
                startActivityForResult(get, 0);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0 && resultCode == RESULT_OK) {
            Intent card = new Intent(this, Cards.class);
            startActivity(card);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            finish();
        }
    }

}