package com.cy.getpoint;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.view.View;
import com.rey.material.app.Dialog;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by CY on 15/7/13.
 */

public class ServerRequests {

    /** ADD BY XGNID 2015 / 8 / 5 **/

    URL url;
    String result;
    User realUser = null;

    /** **/


    Boolean b;
    Context context;
    String errorMsg;
    public static final int CONNECTION_TIMEOUT = 1000 * 15;
    public static final String SERVER_ADDRESS = "http://140.117.169.198";

    public ServerRequests(Context context) {
        this.context = context;
    }

    public Boolean storeUserDataInBackground(User user) {
        try {
            new StoreUserDataAsyncTask(user).execute().get(1000, TimeUnit.MILLISECONDS);
        }catch (Exception e){

        }
        return b;
    }

    public User fetchUserDataAsyncTask(User user) {
        try {
            new fetchUserDataAsyncTask(user).execute().get(1000, TimeUnit.MILLISECONDS);
        }catch (Exception e){

        }
        return realUser;
    }

    public String postDataAsyncTask(String api_url , HashMap<String,String> data ){
        try{
            new postDataAsyncTask( api_url , data   ).execute().get( 1000 , TimeUnit.MILLISECONDS );
        }
        catch( Exception e ){
            e.printStackTrace();
        }
        return result;
    }

    /**
     * parameter sent to task upon execution progress published during
     * background computation result of the background computation
     */

    public class StoreUserDataAsyncTask extends AsyncTask<Void, Void, Void> {
        User user;

        public StoreUserDataAsyncTask(User user) {
            this.user = user;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
            dataToSend.add(new BasicNameValuePair("realname", user.realname));
            dataToSend.add(new BasicNameValuePair("username", user.username));
            dataToSend.add(new BasicNameValuePair("password", user.password));
            dataToSend.add(new BasicNameValuePair("email"   , user.email));
            dataToSend.add(new BasicNameValuePair("nickname", user.nickname));

            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams, CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams, CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post     = new HttpPost(SERVER_ADDRESS + "/api/user/register");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend,"UTF-8"));
                HttpResponse response = client.execute(post);

                HttpEntity entity = response.getEntity();
                String storeResult = EntityUtils.toString(entity);
                JSONObject jObject = new JSONObject(storeResult);

                if (jObject.length() != 0){
                    b = jObject.getBoolean("status");

                    if(!b){
                        errorMsg = jObject.getString("err_msg");
                        System.out.println("Tag" + errorMsg);
                    }

                    else {
                        System.out.println("Tag 成功");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                errorMsg = "Exp";
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try{
                if (!b){
                    final Dialog mDialog = new Dialog(context);
                    mDialog.title(errorMsg);
                    mDialog.positiveAction("好");
                    mDialog.positiveActionClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.cancel();
                        }
                    });
                    mDialog.show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    public class fetchUserDataAsyncTask extends AsyncTask<Void, Void, User> {
        User user;

        public fetchUserDataAsyncTask(User user) {
            this.user = user;
        }

        @Override
        protected User doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
            dataToSend.add(new BasicNameValuePair("username", user.username));
            dataToSend.add(new BasicNameValuePair("password", user.password));

            HttpParams httpRequestParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpRequestParams, CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpRequestParams, CONNECTION_TIMEOUT);

            HttpClient client = new DefaultHttpClient(httpRequestParams);
            HttpPost post = new HttpPost(SERVER_ADDRESS + "/api/user/login");

            try {
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                JSONObject jObject = new JSONObject(result);

                if (jObject.length() != 0){
                    b = jObject.getBoolean("status");

                    if(b){
                        errorMsg = "登入成功";
                        JSONObject data = jObject.getJSONObject("data");

                        realUser = new User(data,user.password);
                    }

                    else{
                        errorMsg = jObject.getString("err_msg");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                errorMsg = "伺服器連線失敗\n請稍候再試！";
                b = false;
            }

            return realUser;
        }

        @Override
        protected void onPostExecute(User returnedUser) {
            super.onPostExecute(returnedUser);

            if(!b){
                final Dialog mDialog = new Dialog(context);
                mDialog.title(errorMsg);
                mDialog.positiveAction("好");
                mDialog.positiveActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.cancel();
                    }
                });
                mDialog.show();
            }
        }
    }

    public static boolean isNetworkAvailable( Context thisContext ){
        ConnectivityManager cm = ( ConnectivityManager ) thisContext.getSystemService(Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetWorkInfo = cm.getActiveNetworkInfo();

        return activeNetWorkInfo != null && activeNetWorkInfo.isConnected();
    }


    public class postDataAsyncTask extends AsyncTask<Void , Void, String> {

        private static final String restErrorMsg = "{status:\"failed\",errorMessage:\"Unable to retrieve API. URL may be invalid.\"}";
        HashMap<String,String> _data;
        String url;

        public postDataAsyncTask( String api_url , HashMap<String,String> data  ){
            _data = data;
            url =  SERVER_ADDRESS + api_url ;

        }

        @Override
        protected String doInBackground(Void... d ) {

            ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

            if( _data != null ){
                for( Map.Entry<String,String> entry : _data.entrySet() ){
                    params.add( new BasicNameValuePair( entry.getKey() , entry.getValue()  ));
                }
            }


            HttpParams hParam = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout( hParam , CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout( hParam , CONNECTION_TIMEOUT);

            HttpClient hc = new DefaultHttpClient( hParam );
            HttpPost hp = new HttpPost( url );

            try{
                hp.setEntity( new UrlEncodedFormEntity( params , HTTP.UTF_8 ));
                HttpResponse res = hc.execute( hp );
                int resCode = res.getStatusLine().getStatusCode();

                if( resCode == 200 ){
                    result = EntityUtils.toString( res.getEntity() );
                }
                else{
                    result = "{status:\"false\",errorMessage:\"status code = "+ resCode +"\"}";
                }

            }
            catch( Exception e ){
                e.printStackTrace();
            }

            return result;
        }
    }

}