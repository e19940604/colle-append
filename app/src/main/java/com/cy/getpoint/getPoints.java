package com.cy.getpoint;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.cy.getpoint.SlidingMenu.ActivityBase;
import com.journeyapps.barcodescanner.CaptureActivity;
import com.cy.getpoint.SlidingMenu.SlidingMenuBuilderConcrete;
import com.rey.material.app.Dialog;

import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class getPoints extends ActivityBase implements View.OnClickListener {
    ImageView p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;
    ImageView[] img = {p1, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};

    int point;
    int position;
    String store_id;
    String user_id;
    String card_id;
    String cards;
    public static final int SCAN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_points);

        try{
            Bundle userBundle = this.getIntent().getExtras();
            String getObject = userBundle.getString("object");

            position = userBundle.getInt("position");
            cards = userBundle.getString("cards");

            JSONObject object = new JSONObject(getObject);
            CardRecord cr = new CardRecord(object);

            p1 = (ImageView) findViewById(R.id.p1);
            p2 = (ImageView) findViewById(R.id.p2);
            p3 = (ImageView) findViewById(R.id.p3);
            p4 = (ImageView) findViewById(R.id.p4);
            p5 = (ImageView) findViewById(R.id.p5);
            p6 = (ImageView) findViewById(R.id.p6);
            p7 = (ImageView) findViewById(R.id.p7);
            p8 = (ImageView) findViewById(R.id.p8);
            p9 = (ImageView) findViewById(R.id.p9);
            p10 = (ImageView) findViewById(R.id.p10);

            initialActionBar(  getSupportActionBar() , cr.storeName + "-" + getString(R.string.card ));

            point = cr.point;
            store_id = cr.storeID;
            user_id = cr.userID;
            card_id = cr.cardID;

            img[1] = p1;
            img[2] = p2;
            img[3] = p3;
            img[4] = p4;
            img[5] = p5;
            img[6] = p6;
            img[7] = p7;
            img[8] = p8;
            img[9] = p9;
            img[10] = p10;

            addPoint(point);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.bChange:  //兌換禮物
                if(point<10){
                    Toast.makeText(this, "還差"+(10-point)+"點才可以兌換喔！", Toast.LENGTH_SHORT).show();
                }

                else {
                    AlertDialog.Builder downloadDialog = new AlertDialog.Builder(this);
                    downloadDialog.setTitle("兌換禮物");
                    downloadDialog.setMessage("請問您是否要兌換禮物？");
                    downloadDialog.setPositiveButton("是",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int i) {
                                    dialog.dismiss();
                                    changeGift();
                                }
                            });

                    downloadDialog.setNegativeButton("下次再換",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int i) {
                                    dialog.dismiss();
                                }
                            });
                    downloadDialog.show();
                }

                break;

            case R.id.bQRcode:
                QRcode(SCAN);
                break;
        }
    }

    void addPoint(int n){   //draw points
        int i;

        if (n>10) {
            n = 10;
        }
        if(n<0){
            n = 0;
            point = 0;
        }

        for (i=1;i<=10;i++){
            img[i].setImageResource(R.mipmap.ic_dot_circle);
        }

        for (i=1;i<=n;i++){
            img[i].setImageResource(R.mipmap.ic_point);
        }
    }

    public void QRcode(int mode) {
        Intent intent = new Intent(this, CaptureActivity.class);
        startActivityForResult(intent, mode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {

        if(requestCode==1 && resultCode==RESULT_OK)
        {

            // ZXing回傳的內容
            String contents = intent.getStringExtra("SCAN_RESULT");
            String s = contents.toString();

            if(s.equals(store_id)){
                Toast.makeText(this, "集點成功！", Toast.LENGTH_SHORT).show();
                addPoint(++point);

                UserSession us = new UserSession(this);
                us.update(cards,store_id,point);

                HashMap map = new HashMap<String,String>();
                ServerRequests add = new ServerRequests(this);

                map.put("user_id", user_id);
                map.put("store_id", store_id);
                add.postDataAsyncTask("/api/addPoint", map);
            }

            else
                Toast.makeText(this, "無法識別", Toast.LENGTH_SHORT).show();
        }

        else
            if(resultCode==RESULT_CANCELED)
                Toast.makeText(this, "取消掃描", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = this.getIntent();
            Bundle bundle = new Bundle();

            bundle.putInt("point",point);
            bundle.putInt("position",position);

            intent.putExtras(bundle);
            setResult(Activity.RESULT_OK, intent);

            finish();

            return super.onKeyDown(keyCode, event);
        }

        return super.onKeyDown(keyCode, event);
    }

    // Your need to put this method in every Activity class where you want to
    // have sliding menu.
    @Override
    public Class<?> setSlidingMenu() {
        // Each activity can have it's own sliding menu controlling builder
        // class.
        return SlidingMenuBuilderConcrete.class;
    }

    @Override
    public boolean enableHomeIconActionSlidingMenu() {
        return true;
    }

    public void changeGift(){
        try{
            HashMap map = new HashMap<String,String>();
            ServerRequests add = new ServerRequests(this);

            map.put("user_id", user_id);
            map.put("card_id", card_id);

            String result = add.postDataAsyncTask("/api/exchangeGift", map);

            JSONObject jObject = new JSONObject(result);

            if (jObject.length() != 0){
                Boolean b = jObject.getBoolean("status");

                if (b) {
                    String numbers = jObject.getString("serialNum");
                    AlertDialog.Builder changeDialog = new AlertDialog.Builder(this);
                    changeDialog.setTitle("兌換流水號");
                    changeDialog.setMessage("請將此流水號告知店員\n\n" + numbers + "\n\n(若要下次兌換請妥善保管流水號！)");
                    changeDialog.setPositiveButton("完成",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int i) {
                                    dialog.dismiss();
                                }
                            });
                    changeDialog.show();

                    point -= 10;
                    addPoint(point);

                    UserSession us = new UserSession(this);
                    us.update(cards,store_id,point);
                }

               else {
                    String errMsg = jObject.getString("err_msg");
                    AlertDialog.Builder errorDialog = new AlertDialog.Builder(this);
                    errorDialog.setMessage("兌換失敗：\n"+errMsg+"\n請稍候再試");
                    errorDialog.setPositiveButton("好",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int i) {
                                    dialog.dismiss();
                                }
                            });
                    errorDialog.show();
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            final Dialog mDialog = new Dialog(this);
            mDialog.title("網路不穩定\n請稍候再試");
            mDialog.positiveAction("好");
            mDialog.positiveActionClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.cancel();
                }
            });
            mDialog.show();
        }
    }
}
