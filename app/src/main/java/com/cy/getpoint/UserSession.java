package com.cy.getpoint;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Xgnid on 2015/8/6.
 */
public class UserSession {

    SharedPreferences sp;

    SharedPreferences.Editor editor;

    Context _context;

    User _user;

    public static final String IS_LOGIN = "isLogin";

    public static final String PREFER_NAME = "reg";

    public static final String KEY_USERNAME = "username";

    public static final String KEY_PASSWORD = "password";

    public static final String KEY_EMAIL = "email";

    public static final String KEY_REALNAME = "realname";

    public static final String KEY_NICKNAME = "nickname";

    public static final String KEY_CARDS = "cards";

    public static final String KEY_USERID = "user_id";

    public static final String KEY_STOREID = "store_id";

    public UserSession( Context context ){
        this._context = context;
        sp = _context.getSharedPreferences( PREFER_NAME , Context.MODE_PRIVATE );
        editor = sp.edit();
    }

    public boolean isLogin(){
        return sp.getBoolean( IS_LOGIN  , false );
    }

    public User getUser(Context context){

        SharedPreferences userSp = context.getSharedPreferences( PREFER_NAME, context.MODE_PRIVATE );
        User realUser = new User();

        realUser.username = userSp.getString("username", "");
        realUser.password = userSp.getString("password", "");
        realUser.user_id = userSp.getString("user_id", "");
        realUser.email = userSp.getString("email", "");
        realUser.realname = userSp.getString("realname", "");
        realUser.nickname = userSp.getString("nickname", "");
        realUser.cards = userSp.getString("cards", "");

        return realUser;
    }

    public void login( User user ){

        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_USERNAME, user.username);

        editor.putString( KEY_PASSWORD , user.password );

        editor.putString( KEY_USERID , user.user_id );

        editor.putString( KEY_REALNAME , user.realname );

        editor.putString( KEY_NICKNAME , user.nickname );

        editor.putString( KEY_EMAIL , user.email );

        editor.putString( KEY_CARDS , user.cards );

        editor.commit();
    }

    public void logout(){
        editor.clear();
        editor.commit();
    }

    public void update(String cards, String store, int point){
        try {
            JSONArray array = new JSONArray(cards);
            CardRecord cr[] = new CardRecord[array.length()];

            for(int n = 0; n < array.length(); n++)
            {
                JSONObject object = array.getJSONObject(n);
                cr[n] = new CardRecord(object);
                if (cr[n].storeID.equals(store)){
                    object.put("point",point);
                    array.put(n,object);
                }

            }
            editor.putString( KEY_CARDS ,  array.toString());
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
