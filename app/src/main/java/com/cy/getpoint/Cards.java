package com.cy.getpoint;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.cy.getpoint.SlidingMenu.ActivityBase;
import com.cy.getpoint.SlidingMenu.SlidingMenuBuilderConcrete;
import com.journeyapps.barcodescanner.CaptureActivity;
import com.rey.material.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cards extends ActivityBase implements View.OnClickListener{
    ListView mainLV;
    SimpleAdapter sa;
    String cards;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards);
        initialActionBar(getSupportActionBar(), getString(R.string.name_en_tw));

        adjustButton(this);

        try {
            UserSession us = new UserSession(this);
            user = us.getUser(this);
            cards = user.cards;

            JSONArray array = new JSONArray(cards);
            CardRecord cr[] = new CardRecord[array.length()];

            for(int n = 0; n < array.length(); n++)
            {
                JSONObject object = array.getJSONObject(n);
                cr[n] = new CardRecord(object);
            }

            homePage(cr);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Your need to put this method in every Activity class where you want to
    // have sliding menu.
    @Override
    public Class<?> setSlidingMenu() {
        // Each activity can have it's own sliding menu controlling builder
        // class.
        return SlidingMenuBuilderConcrete.class;
    }

    @Override
    public boolean enableHomeIconActionSlidingMenu() {
        return true;
    }

    private void homePage(final CardRecord cr[]) {
        mainLV = (ListView) findViewById( R.id.my_card );
        List<Map<String , Object>> items = new ArrayList<Map<String,Object>>();

        for( int i = 0 ; i < cr.length ; ++i ){
            Map<String , Object> item = new HashMap< String , Object >();
            item.put("img", cr[i].img);
            item.put("text" , cr[i].storeName);
            item.put("points", cr[i].point);
            items.add(item);
        }

        sa = new SimpleAdapter( this ,
                items , R.layout.main_simple_adapter_layout , new String[]{"img" , "text" , "points" } ,
                new int[]{ R.id.img , R.id.text , R.id.points }
        );

        mainLV.setAdapter(sa);
        mainLV.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getPoint(cr[position], position);
            }
        });

    }

    public void getPoint(CardRecord cr, int position){
        Intent get = new Intent(this,getPoints.class);

        Bundle userBundle = new Bundle();
        userBundle.putString("cards",cards);
        userBundle.putString("object", cr.object.toString());
        userBundle.putInt("position", position);

        get.putExtras(userBundle);
        startActivityForResult(get, 0);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        /* returned from getPoint */
        if(requestCode == 0 && resultCode == RESULT_OK)
        {
            try {
                UserSession us = new UserSession(this);
                cards = us.getUser(this).cards;

                JSONArray array = new JSONArray(cards);
                CardRecord cr[] = new CardRecord[array.length()];

                for(int n = 0; n < array.length(); n++)
                {
                    JSONObject object = array.getJSONObject(n);
                    cr[n] = new CardRecord(object);
                }

                homePage(cr);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /* returned from QR Code Scanner */
        else if(requestCode==1 && resultCode==RESULT_OK)
        {
            // ZXing回傳的內容
            String contents = intent.getStringExtra("SCAN_RESULT");
            String store = contents.toString();
            Boolean cardExist = false;

            try {
                JSONArray array = new JSONArray(cards);
                CardRecord cr[] = new CardRecord[array.length()];
                int n;

                for(n = 0; n < array.length(); n++)
                {
                    JSONObject object = array.getJSONObject(n);
                    cr[n] = new CardRecord(object);

                    if (cr[n].storeID.equals(store)){

                        cr[n].object.put("point", ++cr[n].point);
                        cardExist = true;

                        UserSession us = new UserSession(this);
                        us.update(cards,cr[n].storeID,cr[n].point);

                        HashMap map = new HashMap<String,String>();
                        ServerRequests add = new ServerRequests(this);

                        map.put("user_id", cr[n].userID);
                        map.put("store_id", cr[n].storeID);
                        add.postDataAsyncTask("/api/addPoint", map);

                        cards = us.getUser(this).cards;
                        Toast.makeText(this, "集點成功！", Toast.LENGTH_LONG).show();

                        break;
                    }
                }

                if (cardExist)
                    getPoint(cr[n],n);

                else {
                    switch (store){
                        case "8BA70CEC":
                        case "5A5AEF7F":
                        case "2A035AB0":
                        case "71649B70":
                        case "2F18E35A": //EASY CARD
                        case "107D8FDC": //Line Card
                            cardExist = true;
                            break;
                        default:
                            Toast.makeText(this, "無法識別", Toast.LENGTH_SHORT).show();
                            break;
                    }

                    if (cardExist){
                        HashMap map = new HashMap<String,String>();
                        ServerRequests add = new ServerRequests(this);

                        System.out.println("Tag UserID:"+user.user_id);
                        System.out.println("Tag Store:" + store);

                        map.put("user_id", user.user_id);
                        map.put("store_id", store);

                        String errorMsg;
                        errorMsg = add.postDataAsyncTask("/api/addPoint", map);

                        System.out.println("Error:" + errorMsg);

                        user = authenticate(user);

                        if (user != null){
                            UserSession us = new UserSession( this );
                            us.login(user);

                            cards = user.cards;

                            array = new JSONArray(cards);
                            CardRecord newCr[] = new CardRecord[array.length()];
                            int i=-1;

                            for(n = 0; n < array.length(); n++)
                            {
                                JSONObject object = array.getJSONObject(n);
                                newCr[n] = new CardRecord(object);

                                if (newCr[n].storeID.equals(store))
                                    i = n;
                            }

                            if (i>=0){
                                Toast.makeText(this, "集點成功！", Toast.LENGTH_LONG).show();
                                getPoint(newCr[i],i);
                            }

                        }
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        else if(requestCode==1 && resultCode==RESULT_CANCELED)
            Toast.makeText(this, "取消掃描", Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intentHome = new Intent(Intent.ACTION_MAIN);
            intentHome.addCategory(Intent.CATEGORY_HOME);
            intentHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentHome);

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bScan:
                QRcode();
                break;
        }
    }

    public void QRcode() {
        Intent intent = new Intent(this, CaptureActivity.class);
        startActivityForResult(intent, 1);
    }

    private User authenticate(User user) {
        ServerRequests serverRequest = new ServerRequests(this);
        return serverRequest.fetchUserDataAsyncTask(user);
    }

    public void adjustButton(Activity activity) {
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            activity.getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;

            if (realHeight > usableHeight){
                Button bScan = (Button) findViewById(R.id.bScan);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bScan.getLayoutParams();

                layoutParams.bottomMargin += realHeight-usableHeight;
                bScan.setLayoutParams(layoutParams);
            }

        }
    }
}
