package com.cy.getpoint;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by CY on 15/7/13.
 */
public class User {

    String realname, username, nickname, password, email, cards;
    String user_id;

    public  User(){
        this.realname = "";
        this.nickname = "";
        this.username = "";
        this.password = "";
        this.email = "";
        this.cards = "";
    }


    public User(String realname, String nickname, String email, String username, String password) {
        this.realname = realname;
        this.nickname = nickname;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(JSONObject data, String password) {
        try {
            JSONObject user = data.getJSONObject("user");
            JSONArray cards = data.getJSONArray("cards");

            this.username = user.getString("username");
            this.user_id = user.getString("user_id");
            this.realname = user.getString("realname");
            this.nickname = user.getString("nickname");
            this.email = user.getString("email");
            this.password = password;

            /* 特別注意，不止一張卡 */
            this.cards = cards.toString();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
