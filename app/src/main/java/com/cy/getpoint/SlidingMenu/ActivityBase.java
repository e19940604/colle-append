package com.cy.getpoint.SlidingMenu;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.cy.getpoint.R;

/**
 * Created by Xgnid on 2015/8/25.
 */
public class ActivityBase extends AppCompatActivity{

    private SlidingMenuBuilderBase slidingMenuBuilderBase;

    @Override
    protected void onCreate( Bundle saveInstanceState ){
        super.onCreate(saveInstanceState);

        createSlidingMenu();

        if( enableHomeIconActionBack() || enableHomeIconActionSlidingMenu()  ){
            ActionBar actionBar = getSupportActionBar();
            if( actionBar != null )
                actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (enableHomeIconActionSlidingMenu() && setSlidingMenu() != null) {
                    slidingMenuBuilderBase.getSlidingMenu().toggle();
                } else if (enableHomeIconActionBack()) {
                    onCustomBackPressed();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // We need to override default behavior of a device back button press if we
    // want to toggle sliding menu.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                onCustomBackPressed();
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    // If sliding menu is showing, we need to hide it on the first back button
    // press.
    private void onCustomBackPressed() {
        if (setSlidingMenu() != null
                && slidingMenuBuilderBase.getSlidingMenu().isMenuShowing()) {
            slidingMenuBuilderBase.getSlidingMenu().toggle();
        } else {
            this.onBackPressed();
        }
    }

    public static void initialActionBar(  ActionBar ab , String title    ){
        /** set actionbar color **/
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2196F3")));
        ab.setTitle(title);
        ab.setHomeAsUpIndicator(R.drawable.menu);
    }

    private void createSlidingMenu(){
        // If nothing is set, than sliding menu wont be created.
        if (setSlidingMenu() != null) {
            Class<?> builder = setSlidingMenu();
            try {
                // We use our made base builder to create a sliding menu.
                slidingMenuBuilderBase = (SlidingMenuBuilderBase) builder
                        .newInstance();
                slidingMenuBuilderBase.createSlidingMenu(this);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Allows to create a sliding out menu in the activity, which extends
     * ActivityBase class, by overriding it and defining controlling builder
     * class.
     *
     * @return Controlling builder class.
     */
    public Class<?> setSlidingMenu() {
        // By default it is set not to create a sliding menu.
        return null;
    }

    /**
     * Sets activity home icon to have up icon and on press act as device back
     * button press.
     *
     * @return Activation state.
     */
    public boolean enableHomeIconActionBack() {
        return false;
    }

    /**
     * Sets activity home icon to be as a sliding menu invoke icon and on press
     * call toggle command for the sliding menu.
     *
     * @return Activation state.
     */
    public boolean enableHomeIconActionSlidingMenu() {
        return false;
    }

    /**
     * Hide keyboard
     */
     @Override
     public boolean onTouchEvent(MotionEvent event) {
         InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                 INPUT_METHOD_SERVICE);

         try {
             imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
         }catch (Exception e){
             e.printStackTrace();
         }

         return true;
     }
}
