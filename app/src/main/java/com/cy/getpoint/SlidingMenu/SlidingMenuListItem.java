package com.cy.getpoint.SlidingMenu;

import com.cy.getpoint.SlidingMenu.SlidingMenuListItem;

/**
 * Created by Xgnid on 2015/8/25.
 */
public class SlidingMenuListItem {
    public int id;
    public String name;
    public int iconResourceId;

    public SlidingMenuListItem(){

    }

    public SlidingMenuListItem( int id, String name , int iconResourceId ){
        this.id = id;
        this.name = name;
        this.iconResourceId = iconResourceId;
    }
}
