package com.cy.getpoint.SlidingMenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cy.getpoint.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xgnid on 2015/8/25.
 */
public class SlidingMenuListAdapter extends ArrayAdapter<SlidingMenuListItem> {

    private Context context;
    private int rowViewResourceId;
    private List<SlidingMenuListItem> slidingMenuListItems = new ArrayList<SlidingMenuListItem>();

    private ImageView slidingMenuItemIcon;
    private TextView slidingMenuItemName;

    public SlidingMenuListAdapter(Context context, int resource , List<SlidingMenuListItem> obj ) {
        super(context, resource);
        this.context = context;
        this.rowViewResourceId = resource;
        this.slidingMenuListItems = obj;

    }

    public int getCount(){
        return this.slidingMenuListItems.size();
    }

    public SlidingMenuListItem getItem( int index ){
        return this.slidingMenuListItems.get(index);
    }

    public View getView( int position , View convertView , ViewGroup parent ){
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(rowViewResourceId, parent,
                    false);
        }

        SlidingMenuListItem slidingMenuListItem = getItem( position );
        slidingMenuItemIcon = (ImageView) row.findViewById(R.id.row_icon);
        slidingMenuItemName = (TextView) row.findViewById(R.id.row_title);

        slidingMenuItemName.setText( slidingMenuListItem.name );

        slidingMenuItemIcon.setImageResource(slidingMenuListItem.iconResourceId  );

        return row;

    }
}
