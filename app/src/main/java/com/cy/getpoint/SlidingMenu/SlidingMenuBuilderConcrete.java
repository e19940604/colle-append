package com.cy.getpoint.SlidingMenu;

import android.widget.Toast;

import com.cy.getpoint.R;

/**
 * Created by Xgnid on 2015/8/25.
 */
public class SlidingMenuBuilderConcrete extends SlidingMenuBuilderBase {

    // We can define actions, which will be called, when we press on separate
    // list items. These actions can override default actions defined inside the
    // base builder. Also, you can create new actions, which will added to the
    // default ones.
    @Override
    public void onListItemClick(SlidingMenuListItem selectedSlidingMenuListItem) {
        super.onListItemClick(selectedSlidingMenuListItem);
    }
}
