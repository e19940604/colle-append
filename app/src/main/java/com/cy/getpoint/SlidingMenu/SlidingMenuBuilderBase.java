package com.cy.getpoint.SlidingMenu;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cy.getpoint.Cards;
import com.cy.getpoint.Login;
import com.cy.getpoint.R;
import com.cy.getpoint.UserSession;
import com.hkm.slidingmenulib.gestured.SlidingMenu;

/**
 * Created by Xgnid on 2015/8/25.
 *
 *         This is base abstract builder class, which is responsible for
 *         creating sliding menu and implementing it's default list items click
 *         actions.
 */
public class SlidingMenuBuilderBase {

    protected Activity activity;
    protected SlidingMenu menu = null;

    /**
     * This method creates sliding out menu from the left screen side. It uses
     * external "SlidingMenu" library for creation. When menu is attached to the
     * activity, it places a list fragment inside the menu as it's content.
     *
     * @param activity
     *            This is Activity to which sliding menu is attached.
     *
     */
    public void createSlidingMenu( Activity activity){
        this.activity = activity;

        menu = new SlidingMenu(activity);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.sliding_menu_shadow_width);
        menu.setShadowDrawable(R.drawable.sliding_menu_shadow );
        menu.setBehindOffsetRes(R.dimen.sliding_menu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(activity, SlidingMenu.SLIDING_WINDOW);
        menu.setMenu(R.layout.side_menu_frame );

        SlidingMenuListFrame slidingMenuListFrame = new SlidingMenuListFrame();
        slidingMenuListFrame.setMenuBuilder(this);

        // We replace a FrameLayout, which is a content of sliding menu, with
        // created list fragment filled with data from menu builder.
        activity.getFragmentManager().beginTransaction()
                .replace(R.id.sliding_menu_frame , slidingMenuListFrame )
                .commit();
    }


    public SlidingMenu getSlidingMenu(){
        return menu;
    }

    // It is our base builder which can be extended, so we can define default
    // actions, which will be called when we press on separate list items.
    public void onListItemClick(SlidingMenuListItem selectedSlidingMenuListItem) {
        CharSequence text;
        switch (selectedSlidingMenuListItem.id) {
            case R.integer.list_item_card_id:
                onSlideMenuCardClick();
                break;
            case R.integer.list_item_logout_id:
                onSlideMenuLogoutClick();
                break;
            case R.integer.list_item_store_id:
                AlertDialog.Builder infoDialog = new AlertDialog.Builder(activity);
                infoDialog.setTitle("集點趣 Loyalty Card App.");
                infoDialog.setMessage("\n軟體版本：5.4.1\n\n" +
                        "程式設計：\n" +
                        "陳重佑\nhi.chungyu@gmail.com\n" +
                        "陳定延\ne19940604@gmail.com\n\n" +
                        "指導老師：陳嘉平 教授\n" +
                        "國立中山大學 資訊工程學系\n" +
                        "104學年度大學部專題製作競賽\n");
                infoDialog.setPositiveButton("確定",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int i) {
                                dialog.dismiss();
                            }
                        });
                infoDialog.show();
                break;
            default:
                text = "Clicked item. default click ";
                Toast.makeText(activity, text, Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void onSlideMenuCardClick() {
        if( activity.getClass().getSimpleName().equals("Cards") ){
            menu.toggle();
            return;
        }


        Intent card = new Intent( activity , Cards.class);
        activity.startActivity( card );
        activity.finish();
    }

    private void onSlideMenuLogoutClick() {
        Toast.makeText(activity, activity.getString(R.string.logging_out) , Toast.LENGTH_LONG).show();
        (new UserSession( activity )).logout();
        Intent login = new Intent( activity , Login.class );
        activity.startActivity( login );
        activity.finish();
    }

}
