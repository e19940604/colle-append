package com.cy.getpoint.SlidingMenu;

import android.app.Activity;

import com.cy.getpoint.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xgnid on 2015/8/25.
 */
public class SlidingMenuList {

    public static final List<SlidingMenuListItem> getSlidingMenu( Activity activity ){

        List<SlidingMenuListItem> list = new ArrayList<SlidingMenuListItem>();
        list.add( new SlidingMenuListItem( R.integer.list_item_card_id ,
                activity.getResources().getString( R.string.list_item_card_label),
                R.drawable.list_item_card_icon  ));

        list.add( new SlidingMenuListItem( R.integer.list_item_store_id ,
                activity.getResources().getString( R.string.info) ,
                R.drawable.info));

        list.add( new SlidingMenuListItem( R.integer.list_item_logout_id ,
                activity.getResources().getString( R.string.list_item_logout_label),
                R.drawable.list_item_logout_icon  ));


        return list;
    }
}
