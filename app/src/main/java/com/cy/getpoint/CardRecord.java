package com.cy.getpoint;

import org.json.JSONObject;

/**
 * Created by CY on 15/9/3.
 */
public class CardRecord {
    int point;
    String storeID, storeName, userID, cardID;
    JSONObject object;
    int img;

    public CardRecord(JSONObject object){
        this.object = object;

        try {
            point = object.getInt("point");
            storeID = object.getString("store_id");
            cardID = object.getString("card_id");
            userID = object.getString("user_id");

            switch (storeID){
                case "8BA70CEC":    //WHSH
                    storeName = "吳家紅茶冰";
                    img = R.drawable.p1_new;
                    break;
                case "5A5AEF7F":    //高捷
                    storeName = "威爾希斯";
                    img = R.drawable.p3_new;
                    break;
                case "2A035AB0":    //SONY
                    storeName = "双妃奶茶";
                    img = R.drawable.p2_new;
                    break;
                case "71649B70":
                    storeName = "3M Tag For Test";
                    break;
                case "2F18E35A":    //EASY CARD
                    storeName = "抽獎活動1";
                    img = R.drawable.lottery1;
                    break;
                case "107D8FDC":    //Line Card
                    storeName = "抽獎活動2";
                    img = R.drawable.lottery2;
                    break;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
