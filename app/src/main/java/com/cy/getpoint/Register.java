package com.cy.getpoint;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.cy.getpoint.SlidingMenu.ActivityBase;
import com.rey.material.app.Dialog;


public class Register extends ActivityBase implements View.OnClickListener{
    EditText etName, etEmail, etUsername;
    EditText etPasswordAgain, etPassword, etNickname;
    Button bRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initialActionBar(getSupportActionBar(), getString(R.string.register_tw ) );

        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPasswordAgain = (EditText) findViewById(R.id.etPasswordAgain);
        etNickname = (EditText) findViewById(R.id.etNickName);

        bRegister = (Button) findViewById(R.id.bRegister);
        bRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String errorMsg;
        final Dialog mDialog = new Dialog(this);

        switch (v.getId()) {
            case R.id.bRegister:
                String name     = etName.getText().toString();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String passwordAgain = etPasswordAgain.getText().toString();
                String nickname = etNickname.getText().toString();
                String email    = etEmail.getText().toString();

                if (!password.equals(passwordAgain)){
                    errorMsg = "密碼不符，請重新輸入！";
                    mDialog.title(errorMsg);
                    mDialog.positiveAction("好");
                    mDialog.positiveActionClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.cancel();
                        }
                    });
                    mDialog.show();
                    break;
                }

                User user = new User(name, nickname, email, username, password);
                Boolean b = registerUser(user);

                /*
                是否改為直接開啟卡片頁面？
                 */

                try{
                    if (b){
                        Intent loginIntent = new Intent(Register.this, Login.class);
                        startActivity(loginIntent);
                        Toast.makeText(v.getContext(), "註冊成功！", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    break;
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(v.getContext(), "伺服器連線失敗！", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private Boolean registerUser(User user) {
        ServerRequests serverRequest = new ServerRequests(this);
        return serverRequest.storeUserDataInBackground(user);
    }
}
