package com.cy.getpoint;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.view.KeyEvent;
import com.cy.getpoint.SlidingMenu.ActivityBase;

public class Login extends ActivityBase implements View.OnClickListener {
    EditText etUsername, etPassword;
    User realUser = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);

        ActionBar ab = getSupportActionBar();
        ab.hide();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bLogin:
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                User user = new User(username, password);
                realUser = authenticate(user); //認證＋擷取資料

                if (realUser != null){
                    UserSession us = new UserSession( this );
                    us.login(realUser);

                    Toast.makeText(view.getContext(), "登入成功！", Toast.LENGTH_SHORT).show();

                    Intent card = new Intent(this, Cards.class);
                    Bundle userBundle = new Bundle();

                    userBundle.putString("cards",realUser.cards);
                    card.putExtras(userBundle);

                    startActivity(card);
                    finish();
                }

                break;

            case R.id.tvRegisterLink:
                Intent registerIntent = new Intent(Login.this, Register.class);
                startActivity(registerIntent);
                break;
        }
    }

    private User authenticate(User user) {
        ServerRequests serverRequest = new ServerRequests(this);
        return serverRequest.fetchUserDataAsyncTask(user);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intentHome = new Intent(Intent.ACTION_MAIN);
            intentHome.addCategory(Intent.CATEGORY_HOME);
            intentHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentHome);

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}

